#!/bin/bash
#mv -v $dir/*{M,m}ieow*.{rar,zip} $dirrustle/
#mv -v $dir/*{R,r}ustle*.{rar,zip} $dirrustle/
#mv -v $dir/*
#unrar t file.rar

dir=/var/run/media/lo/Doujins

mv -v $dir/*rustle* $dir/rustle
mv -v $dir/*Rustle* $dir/rustle
mv -v $dir/*mieow* $dir/rustle
mv -v $dir/*Mieow* $dir/rustle
for i in $dir/rustle/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/rustle/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*henreader* $dir/henreader
mv -v $dir/*Henreader* $dir/henreader
for i in $dir/henreader/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/henreader/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*maeshima\ Ryo* $dir/maeshima-ryou
mv -v $dir/*Maeshima\ ryo* $dir/maeshima-ryou
for i in $dir/maeshima-ryou/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/maeshima-ryou/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*inuboshi* $dir/inuboshi
mv -v $dir/Iinuboshi* $dir/inuboshi
for i in $dir/inuboshi/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/inuboshi/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*ponsuke* $dir/ponsuke
mv -v $dir/*Ponsuke* $dir/ponsuke
for i in $dir/ponsuke/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/ponsuke/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*quzilax* $dir/quzilax
mv -v $dir/*Quzilax* $dir/quzilax
for i in $dir/quzilax/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/quzilax/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt
mv -v $dir/*fatalpulse* $dir/fatalpulse
mv -v $dir/*Fatalpulse* $dir/fatalpulse
for i in $dir/fatalpulse/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/fatalpulse/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*yukiu* $dir/yukiu-con
mv -v $dir/*Yukiu* $dir/yukiu-con
for i in $dir/yukiu-con/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/yukiu-con/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*rotary* $dir/rotary-engine
mv -v $dir/*Rotary* $dir/rotary-engine
for i in $dir/rotary-engine/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/rotary-engine/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*ootsuka* $dir/ootsuka-reika
mv -v $dir/*Ootsuka* $dir/ootsuka-reika
for i in $dir/ootsuka-reika/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/ootsuka-reika/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*mizu* $dir/mizu
mv -v $dir/*Mizu* $dir/mizu
for i in $dir/mizu/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/mizu/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*Service\ Heaven* $dir/service-heaven
mv -v $dir/*Service\ heaven* $dir/service-heaven
mv -v $dir/*service\ heaven* $dir/service-heaven
mv -v $dir/*service\ Heaven* $dir/service-heaven
for i in $dir/service-heaven/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/service-heaven/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*X-EROS* $dir/x-eros
mv -v $dir/*x-eros* $dir/x-eros
for i in $dir/x-eros/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/x-eros/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*fuyuno* $dir/fuyuno-mikan
mv -v $dir/*Fuyuno* $dir/fuyuno-mikan
for i in $dir/funyuno-mikan/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/funyuno-mikan/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*as109* $dir/as109
mv -v $dir/*As109* $dir/as109
for i in $dir/as109/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/as109/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*bubukka* $dir/bubukka
mv -v $dir/*Bubukka* $dir/bubukka
for i in $dir/bubukka/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/bubukka/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*\[yam\]* $dir/yam
mv -v $dir/*\[Yam\]* $dir/yam
for i in $dir/yam/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/yam/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*shimimaru* $dir/Shimimaru
mv -v $dir/*Shimimaru* $dir/Shimimaru
for i in $dir/Shimimaru/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/Shimimaru/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*we53* $dir/we53
mv -v $dir/*we53* $dir/we53
for i in $dir/we53/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/we53/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*senke\ kagero* $dir/senke-kagero
mv -v $dir/*Senke\ kagero* $dir/senke-kagero
mv -v $dir/*senke\ Kagero* $dir/senke-kagero
mv -v $dir/*Senke\ Kagero* $dir/senke-kagero
for i in $dir/senke-kagero/*.zip ; do unzip -t "$i" ; done | grep "At least" >> $HOME/errors.txt
for i in $dir/senke-kagero/*.zip ; do unrar t "$i" ; done | grep "At least" >> $HOME/errors.txt

mv -v $dir/*kawasaki* $dir/Kawasaki 2>/dev/null
mv -v $dir/*Kawasaki* $dir/Kawasaki 2>/dev/null
for i in $dir/Kawasaki/*.zip ; do unzip -t "$i" 2>$HOME/errors.txt ; done
for i in $dir/Kawasaki/*.zip ; do unrar t "$i" 2>$HOME/errors.txt ; done

mv -v $dir/*higashiyama\ show* $dir/higashiyama-show 2>/dev/null
mv -v $dir/*Higashiyama\ Show* $dir/higashiyama-show 2>/dev/null
mv -v $dir/*higashiyama\ Show* $dir/higashiyama-show 2>/dev/null
mv -v $dir/*Higashiyama\ show* $dir/higashiyama-show 2>/dev/null
for i in $dir/higashiyama-show/*.zip ; do unzip -t "$i" 2>$HOME/errors.txt ; done
for i in $dir/higashiyama-show/*.zip ; do unrar t "$i" 2>$HOME/errors.txt ; done
