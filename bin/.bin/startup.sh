#!/bin/bash
#Auto Startup script

#launch three terminals on the first workspace
#newsbeuter
xdotool key super+1
sleep 1

urxvt -e "newsbeuter" & disown
sleep 2
xdotool key R
sleep 1
xdotool key t

#mpd+ncmpcpp
urxvt & disown
sleep 1
xdotool type "pkill mpd"
xdotool key Return
sleep 3
xdotool type "mpd"
xdotool key Return
sleep 5
xdotool type "ncmpcpp"
xdotool key Return
sleep 1
xdotool key 4

#ranger
sleep 1
urxvt -e "ranger" & disown
sleep 4
xdotool key g n
sleep 1
xdotool key g v
sleep 1
xdotool type 10
xdotool key Down Right
sleep 1
xdotool key g n
sleep Tab

#firefox
#xdotool key super+2
#sleep 1
#xdotool key super+space
#sleep 1
#xdotool type "firefox"
#xdotool key Return
#sleep 12

#Update my git repos + rtorrent
xdotool key super+3
sleep 1
urxvt & disown
sleep 1
xdotool type "cd ~/.misc/git/misc && sh stow.sh | less --max-back-scroll=2000 "
xdotool key Return
sleep 3
urxvt & disown
sleep 1
xdotool type "arch"
xdotool key Return
sleep 1

urxvt -e "rtorrent" & disown
sleep 1
xdotool key 5
sleep 1
xdotool key Super+ctrl+j
sleep 1
urxvt & disown
sleep 1
xdotool type "cd .misc/git/stow"
xdotool key Return
xdotool type "sh stow.sh"
xdotool key Return

#pavucontrol + Steam
xdotool key super+4
sleep 1
pavucontrol & disown
steam & disown

#replace steam with steam & disown

#tmux session on workspace 5
sleep 1
xdotool key super+5
sleep 1
urxvt -e "tmux" & disown
sleep 10
xdotool type "vim ~/.bin/startup.sh"
xdotool key Return
sleep 10
xdotool key colon
xdotool type "Minimap"
xdotool key Return
sleep 1

#brings me back to the first workspace
sleep 1
xdotool key super+1

