#!/bin/bash
#########
#FOLDERS#
#########


ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"


if [ -d "$HOME/dotfiles" ]; then
        echo -e "$COL_RED ~/dotfiles exists,No need to download it $COL_RESET"
else
        echo -e "$COL_GREEN Dotfiles doesn't exist,downloading... $COL_RESET"
        git clone git@gitlab.com:lo-YHF/dotfiles.git
fi

cd $HOME/dotfiles

if [ ! -d "$HOME/.torrents" ]; then
        echo -e "$COL_GREEN ~/.torrents doesn't exist,Creating $COL_RESET"
        mkdir $HOME/.torrents
else
        echo -e "$COL_RED ~/.torrents exist $COL_RESET"
fi

if [ ! -d "$HOME/.rtorrent" ]; then
        echo -e "$COL_GREEN ~/.rtorrent does't exist,Creating $COL_RESET"
        mkdir $HOME/.rtorrent
        mkdir $HOME/.downloads
else
        echo -e "$COL_RED ~/.rtorrent exists $COL_RESET"
fi

if [ ! -d "$HOME/.config/dunst" ]; then
        echo -e "$COL_GREEN ~/.config/dunst doesn't exist,Creating $COL_RESET"
        stow dunst
else
        echo -e "$COL_RED ~/.config/dunst exists $COL_RESET"
fi

if [ ! -d "$HOME/.ncmpcpp" ]; then
        echo -e "$COL_GREEN ~/.ncmpcpp doesn't exist,Creating $COL_RESET"
        stow ncmpcpp
else
        echo -e "$COL_RED ~/.ncmpcpp exists $COL_RESET"
fi

if [ ! -d "$HOME/.config/bspwm" ]; then
        echo -e "$COL_GREEN ~/.config/bspwm doesn't exist,Creating $COL_RESET"
        stow bspwm
else
        echo -e "$COL_RED ~/.config/bspwm exists $COL_RESET"
fi

if [ ! -d "$HOME/.bin" ]; then
        echo -e "$COL_GREEN ~/.bin doesn't exist,Creating $COL_RESET"
        stow bin
else
        echo -e "$COL_RED ~/.bin exists $COL_RESET"
fi

if [ ! -d "$HOME/.config/ranger" ]; then
        echo -e "$COL_GREEN ~/.config/ranger doesn't exists,Creating $COL_RESET"
        stow ranger
else
        echo -e "$COL_RED ~/.config/ranger exists $COL_RESET"
fi

if [ ! -d "$HOME/.panel" ]; then
        echo -e "$COL_GREEN ~/.panel doesn't exist,Creating $COL_RESET"
        stow panel
else
        echo -e "$COL_RED ~/.panel exists $COL_RESET"
fi

if [ ! -d "$HOME/.config/sxhkd" ]; then
        echo -e "$COL_GREEN ~/.config/sxhkd doesn't exist,Creating $COL_RESET"
        stow sxhkd
else
        echo -e "$COL_RED ~/.config/sxhkd exists $COL_RESET"
fi

if [ ! -d "$HOME/.config/vimb" ]; then
        echo -e "$COL_GREEN ~/.config/vimb doesn't exist,Creating $COL_RESET"
        stow vimb
else
        echo -e "$COL_RED ~/.config/vimb exists $COL_RESET"
fi

if [ ! -d "$HOME/.newsbeuter" ]; then
        echo -e "$COL_GREEN ~/.newsbeuter doesn't exist,Creating $COL_RESET"
        stow newsbeuter
else
        echo -e "$COL_RED ~/.newsbeuter exists $COL_RESET"
fi

if [ ! -d "$HOME/.config/vis" ]; then
        echo -e "$COL_GREEN ~/.config/vis doesn't exist,Creating"
        stow vis
else
        echo -e "$COL_RED ~/.config/vis exists $COL_RESET"
fi

if [ ! -d "$HOME/.config/obs-studio/" ]; then
        echo -e "$COL_GREEN ~/config/obs-studio/ doesn't exist,Creating"
        stow obs
else
        echo -e "$COL_RED ~/.config/obs-studio/ exists $COL_RESET"
fi



#######
#FILES#
#######

if [ ! -f "$HOME/.Xresources" ]; then
        echo -e "$COL_GREEN ~/.Xresources doesn't exist,Creating $COL_RESET"
        stow xresources && xrdb -merge ~/.Xresources
else
        echo -e "$COL_RED ~/.Xresources exists $COL_RESET"
fi

if [ ! -f "$HOME/.compton.conf" ]; then
        echo -e "$COL_GREEN ~/.compton.conf doesn't exist,Creating $COL_RESET"
        stow compton
else
        echo -e "$COL_RED ~/.compton.conf exists $COL_RESET"
fi

if [ ! -f "$HOME/.mpdconf" ]; then
        echo -e "$COL_GREEN ~/.mpdconf doesn't exist,Creating $COL_RESET"
        stow mpd && pkill mpd && mpd
else
        echo -e "$COL_RED ~/.mpdconf exists $COL_RESET"
fi

if [ ! -f "$HOME/.rtorrent.rc" ]; then
        echo -e "$COL_GREEN ~/.rtorrent.rc doesn't exist,Creating $COL_RESET"
        stow rtorrent
else
        echo -e "$COL_RED ~/.rtorrent.rc exists $COL_RESET"
fi

if [ ! -f "$HOME/.tmux.conf" ]; then
        echo -e "$COL_GREEN ~/.tmux.conf doesn't exist,Creating $COL_RESET"
        stow tmux
else
        echo -e "$COL_RED ~/.tmux.conf exists $COL_RESET"
fi

if [ ! -f "$HOME/.vimrc" ]; then
        echo -e "$COL_GREEN ~/.vimrc doesn't exist,Creating $COL_RESET"
        stow vim
else
        echo -e "$COL_RED ~/.vimrc exists $COL_RESET"
fi

if [ ! -d "$HOME/.vim/" ]; then
        echo -e "$COL_GREEN ~/.vim doesn't exist,Creating $COL_RESET"
        mkdir -p $HOME/.vim/bundle && git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.vim/bundle/Vundle.vim
else
        echo -e "$COL_RED ~/.vim/ exists and so does vundle $COL_RESET"
fi

if [ ! -f "$HOME/.xinitrc" ]; then
        echo -e "$COL_GREEN ~/.xinitrc doesn't exist,Creating $COL_RESET"
        stow xinitrc
else
        echo -e "$COL_RED ~/.xinitrc $COL_RESET"
fi

if [ ! -f "$HOME/.zshrc" ]; then
        echo -e "$COL_GREEN ~/.zshrc doesn't exist,Creating $COL_RESET"
        stow zsh
else
        echo -e "$COL_RED ~/.zshrc exists $COL_RESET"
fi

if [ ! -f "$HOME/.local/share/fonts/Tewi-normal-11.bdf" ]; then
        echo -e "$COL_GREEN ~/.local/share/fonts/Tewi-normal-11.bdf doesn't exist,Creating $COL_RESET"
        stow fonts
else
        echo -e "$COL_RED ~/.local/share/fonts/Tewi fonts exist $COL_RESET"
fi


#test if noto exists and create if non existant using noto script
