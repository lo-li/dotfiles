export ZSH=/home/lo/.antigen/repos/https-COLON--SLASH--SLASH-github.com-SLASH-robbyrussell-SLASH-oh-my-zsh.git
export PANEL_FIFO PANEL_HEIGHT PANEL_FONT_FAMILY
ZSH_THEME="flazz"
set EDITOR="vim"
set TERMCMD="urxvt"
set SHELL="/bin/zsh"
BROWSER="vimb"
export UPDATE_ZSH_DAYS=4
ENABLE_CORRECTION="false"
COMPLETION_WAITING_DOTS="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"
plugins=(git sudo zsh-completions)
 export PATH="/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/home/lo/.panel:/home/lo/.bin:/home/lo/.gem/ruby/2.2.0/bin:/usr/bin/core_perl:/home/lo/.qfc/bin"
source $ZSH/oh-my-zsh.sh
export LANG=en_IE.UTF-8
autoload -U compinit && compinit

alias rm="trash-put"
alias trash="trash-put"
alias empty="trash-empty"
alias tl="trash-list"
alias install="sudo pacman -S"
alias remove="sudo pacman -Rns"
alias orphan="sudo pacman -Rns $(pacman -Qtdq)"

source /usr/share/autoenv-git/activate.sh
